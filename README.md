**String Calculator**

Write a java program for the below problem statement. 
Your Java program should be as per the standards:

1. Your solution should be a maven/gradle/ant project
2. Follow TDD( Test Driven Development ) principles
3. Make necessary assumptions where required, mention any speciﬁcassumptions
4. Solution will be judged basis extensibility ,scalability, exceptionhandling and use of proper design and OOPS principles.
5. Use Java 8 features where possible.
6. Please do not google, we appreciate originality of thoughts

After you are done with the assignment, please checkin your code to GIT repository under your user proﬁle by creating a new repository and share the link with your recruitment contact.

---

**Problem**

John is a software programmer and he is trying to develop a calculator program. He feels that normal calculators are boring as they dont allow to have input as a full mathematical expression, rather it allows is a sequence of consecutive operands and operators. John is aware of scientiﬁc calculators which have support for complex mathematical expressions. However he is much interested to write his own program which can accept a simple to complex mathematical expression and give the output of it.

The mathematical expression can have operators and operands. Operators can be + - \ * ^ ( ) and operands can be number between 0-9 . A sample mathematical expression will look as 7 + (6 * 5^2 + 3-4/2) . A mathematical expression cannot start or end with a operator with only exception for ( (expression can start but cannot end) and ) (expression can end but cannot start). Every opening brace ( must have a corresponding closing brace ) . An operand cannot have its adjacent as a operator except for ( i.e (( and ) i.e ))

---

**Input**

The ﬁrst line of the input gives the number of test cases, T. T test cases follow. Each consists of one line with a string S. S represents the mathematical expression which will the input for the string calculator

---

**Output**

For each test case, output one line containing Case #x: y, where x is the test case number (starting from 1) and y is either INVALID EXPRESSION if input string is invalid , or an integer representing the value of the mathematical expression after calculation

---

**Limits**

1 ≤ T ≤ 100.

In case of out put as decimals, print values only upto 2 decimals

---

**Sample**

---

**Input**

---

4

7+(6*5^2+3-4/2)

7+(67(56*2))

8*+7

(8*5/8)-(3/1)-5


---

**Output**

Case #1: 158

Case #2: INVALID EXPRESSION

Case #3: INVALID EXPRESSION

Case #4: -3


---

Please find the checkin code of above assignment with my below repository links : 

https://bitbucket.org/saibol/calculator/src/master/

https://github.com/saibol/calculator/tree/master

---

**About Project Developed**

- Used Maven along with Log4j and JDK 1.8
- Follwed TDD approach : More than 50+ testes written
- Implemented Design principles like Single Responsibility, OpenClose Principle etc.
- Used design patters such as Singleton, Factory, Rule Design Pattern and wrote code losed coupled
- Used Rule Based design pattern while validation, considering extensibility factor
- Used DataStructures such as Stack, List while rule evalution


---

**List of assumptions made**

1) If no expression is passed or blank spaces are send, it will be considered as invalid expression

2) Expression starting/ending with decimal point (.) will be considered invalid expression 

3) expression starting with minus (-) will also be considered invalid i.e. -2+(5+6)

4) Any digit between 0-9 : considered meaning as single as well more number places i.e. 200 is valid input and digits with decimal places will be invalid


---

**Testing Evidences**

	all-test-cases-passed.jpg
	
	response-received-for-console-entry.jpg
	
	evaluation-bm-test-cases-response.jpg
	

---

**Class Diagram** 

	string-calculator-class-diagram.jpg
