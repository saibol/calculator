package com.sai.cal.bm;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.sai.cal.constant.CalulatorConstant;
import com.sai.cal.model.Expression;
import com.sai.cal.model.RuleResult;

public class RuleBMTest {

	private RuleBM ruleBM = RuleBM.getInstance();
	List<Expression> _exprs = null;

	@Before
	public void init() {
		_exprs = Arrays.asList(
				new Expression("1+23+5"), 
				new Expression("1+23+5++"), 
				new Expression("1**&++2"),
				new Expression("1+(2-5)"));
	}

	@Test
	public void process_checkResultSize() {
		List<RuleResult> results = ruleBM.process(_exprs);
		assertEquals(4, results.size());
	}
	
	@Test
	public void process_checkResultContent() {
		List<RuleResult> results = ruleBM.process(_exprs);
		assertEquals("29", results.get(0).getResult());
		assertEquals(CalulatorConstant.MSG_EXPR_INVALID, results.get(1).getResult());
		assertEquals(CalulatorConstant.MSG_EXPR_INVALID, results.get(2).getResult());
	}
	
//	@Test
//	public void execute_valid() {
//		assertEquals(5, ruleBM.execute(new Expression("1+5*8/7-2")));
//	}
}
