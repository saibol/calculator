package com.sai.cal.bm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.sai.cal.factory.ComponentFactory;
import com.sai.cal.model.Component;
import com.sai.cal.model.Number;

public class EvaluatorBMTest {

	List<Component> infixComps;
	private EvaluatorBM evaluatorBM = EvaluatorBM.getInstance();

	@Test
	public void infxToPostfx_postfixSize() {
		infixComps = ComponentFactory.createComponents("7+(6*5^2+3-4/2)");
		List<Component> postfixComps = evaluatorBM.infxToPostfx(infixComps);
		assertEquals(13, postfixComps.size());
	}
	
	@Test
	public void infxToPostfx_singleBrace() {
		infixComps = ComponentFactory.createComponents("7+(6*5^2+3-4/2)");
		List<Component> postfixComps = evaluatorBM.infxToPostfx(infixComps);
		assertEquals("7652^*3+42/-+", evaluatorBM.getExpr(postfixComps));
	}
	
	@Test
	public void infxToPostfx_multipleBracesWithMultipleSub() {
		infixComps = ComponentFactory.createComponents("(8*5/8)-(3/1)-5");
		List<Component> postfixComps = evaluatorBM.infxToPostfx(infixComps);
		assertEquals("85*8/31/-5-", evaluatorBM.getExpr(postfixComps));
	}
	
	@Test
	public void getExpr_multipleBracesWithPower() {
		infixComps = ComponentFactory.createComponents("2^3^2");
		List<Component> postfixComps = evaluatorBM.infxToPostfx(infixComps);
		assertEquals("232^^", evaluatorBM.getExpr(postfixComps));
	}
	
	@Test
	public void evaluate_postfixSize() {
		infixComps = ComponentFactory.createComponents("7+(6*5^2+3-4/2)");
		List<Component> postfixComps = evaluatorBM.infxToPostfx(infixComps);
		Number _cmp = (Number) evaluatorBM.evaluate(postfixComps);
		assertEquals(new Integer(158), _cmp.getNumber());
	}

	@Test
	public void evaluate_multipleBracesWithMultipleSub() {
		infixComps = ComponentFactory.createComponents("(8*5/8)-(3/1)-5");
		List<Component> postfixComps = evaluatorBM.infxToPostfx(infixComps);
		Number _cmp = (Number) evaluatorBM.evaluate(postfixComps);
		assertEquals(new Integer(-3), _cmp.getNumber());
	}

	@Test
	public void evaluate_multipleBracesWithPower() {
		infixComps = ComponentFactory.createComponents("2^3^2");
		List<Component> postfixComps = evaluatorBM.infxToPostfx(infixComps);
		Number _cmp = (Number) evaluatorBM.evaluate(postfixComps);
		assertEquals(new Integer(512), _cmp.getNumber());
	}
	
	@Test
	public void calculate_add() {
		Component op = ComponentFactory.getComponent("+");
		Component num1 = ComponentFactory.getComponent("1");
		Component num2 = ComponentFactory.getComponent("2");
		assertEquals(new Integer(3), evaluatorBM.calculate(op, num1, num2));
	}
	
	@Test
	public void calculate_mul() {
		Component op = ComponentFactory.getComponent("*");
		Component num1 = ComponentFactory.getComponent("5");
		Component num2 = ComponentFactory.getComponent("2");
		assertEquals(new Integer(10), evaluatorBM.calculate(op, num1, num2));
	}
	
	@Test
	public void calculate_sub() {
		Component op = ComponentFactory.getComponent("-");
		Component num1 = ComponentFactory.getComponent("5");
		Component num2 = ComponentFactory.getComponent("20");
		assertEquals(new Integer(15), evaluatorBM.calculate(op, num1, num2));
	}
	@Test
	public void hasStkHigherPriorityEle_subAndMul() {
		Component subOp = ComponentFactory.getComponent("-");
		Component mulOp = ComponentFactory.getComponent("*");
		assertTrue(evaluatorBM.hasStkHigherPriorityEle(subOp, mulOp));
	}
	
	@Test
	public void hasStkHigherPriorityEle_bothSub() {
		Component subOp = ComponentFactory.getComponent("-");
		assertTrue(evaluatorBM.hasStkHigherPriorityEle(subOp, subOp));
	}
	
	@Test
	public void hasStkHigherPriorityEle_bothPow() {
		Component subOp = ComponentFactory.getComponent("^");
		assertFalse(evaluatorBM.hasStkHigherPriorityEle(subOp, subOp));
	}
}
