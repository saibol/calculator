package com.sai.cal.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ExpressionTest {

	Expression expression;

	@Test
	public void setContent_spacesPresentInContent() {
		expression = new Expression("7+(  65 + 0)");
		assertEquals("7+(65+0)", expression.getContent());
	}

	@Test
	public void setContent_spacesNotPresentInContent() {
		expression = new Expression("7+(65+0)");
		assertEquals("7+(65+0)", expression.getContent());
	}

	@Test
	public void setContent_containsOnlySpaces() {
		expression = new Expression("     ");
		assertEquals("", expression.getContent());
	}
}
