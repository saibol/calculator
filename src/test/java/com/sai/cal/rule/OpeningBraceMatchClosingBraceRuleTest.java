package com.sai.cal.rule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sai.cal.model.Expression;

public class OpeningBraceMatchClosingBraceRuleTest {

	private static ICalculatorRule rule;

	@BeforeClass
	public static void init() {
		rule = new OpeningBraceMatchClosingBraceRule();
	}

	@Test
	public void isValid_moreOpeningThanClosingBraces() {
		Expression expr = new Expression("(2+(7+4)");
		assertFalse(rule.isValid(expr));
	}

	@Test
	public void isValid_lessOpeningThanClosingBraces() {
		Expression expr = new Expression("2+(7+4))");
		assertFalse(rule.isValid(expr));
	}

	@Test
	public void isValid_equalOpeningAndClosingBraces() {
		Expression expr = new Expression("2+(7+4)");
		assertTrue(rule.isValid(expr));
	}

}
