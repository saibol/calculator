package com.sai.cal.rule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sai.cal.model.Expression;

public class AdjacentToClosingBraceRuleTest {

	private static ICalculatorRule rule;

	@BeforeClass
	public static void init() {
		rule = new AdjacentToClosingBraceRule();
	}

	@Test
	public void isValid_openingBraceAfterClosing() {
		Expression exprBasic = new Expression("1+(2+6)(1+2)");
		assertFalse(rule.isValid(exprBasic));
	}

	@Test
	public void isValid_decimalPointAfterClosingBrace() {
		Expression exprBasic = new Expression("1+(2+6).1+2");
		assertFalse(rule.isValid(exprBasic));
	}

	@Test
	public void isValid_validExpression() {
		Expression expr = new Expression("7+(6*5^2+3-4/2)");
		assertTrue(rule.isValid(expr));
	}
}
