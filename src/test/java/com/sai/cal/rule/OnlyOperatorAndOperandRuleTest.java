package com.sai.cal.rule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sai.cal.model.Expression;

public class OnlyOperatorAndOperandRuleTest {

	private static ICalculatorRule rule;

	@BeforeClass
	public static void init() {
		rule = new OnlyOperatorAndOperandRule();
	}

	@Test
	public void isValid_invalidExpression() {
		Expression expr = new Expression("2+(7+4)&&**");
		assertFalse(rule.isValid(expr));
	}

	@Test
	public void isValid_validExpression() {
		Expression expr = new Expression("2+(7+450)");
		assertTrue(rule.isValid(expr));
	}

	@Test
	public void isValid_blankExpression() {
		Expression expr = new Expression(" ");
		assertTrue(rule.isValid(expr));
	}

}
