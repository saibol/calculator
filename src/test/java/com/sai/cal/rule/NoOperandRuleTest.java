package com.sai.cal.rule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sai.cal.model.Expression;

public class NoOperandRuleTest {

	private static ICalculatorRule rule;

	@BeforeClass
	public static void init() {
		rule = new NoOperandRule();
	}

	@Test
	public void isValid_allOperatorsInExpression() {
		Expression expr = new Expression("+(+*-");
		assertFalse(rule.isValid(expr));
	}

	@Test
	public void isValid_validExprWithOperatorsAndOperand() {
		Expression expr = new Expression("1+(2+3)*5");
		assertTrue(rule.isValid(expr));
	}

}
