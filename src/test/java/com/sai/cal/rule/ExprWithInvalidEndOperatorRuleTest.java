package com.sai.cal.rule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sai.cal.model.Expression;

public class ExprWithInvalidEndOperatorRuleTest {

	private static ICalculatorRule rule;

	@BeforeClass
	public static void init() {
		rule = new ExprWithInvalidEndOperatorRule();
	}

	@Test
	public void isValid_endsWithOpeningRoundBracket() {
		Expression expr = new Expression("2+(7+4)(");
		assertFalse(rule.isValid(expr));
	}
	
	@Test
	public void isValid_endsWithOperator() {
		Expression expr = new Expression("2+(7+4)+");
		assertFalse(rule.isValid(expr));
	}

	@Test
	public void isValid_endsWithDecimalPoint() {
		Expression expr = new Expression("2+(7+4).");
		assertFalse(rule.isValid(expr));
	}

	@Test
	public void isValid_endsWithClosingRoundBracket() {
		Expression expr = new Expression("3+(2+7)");
		assertTrue(rule.isValid(expr));
	}

}
