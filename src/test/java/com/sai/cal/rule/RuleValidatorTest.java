package com.sai.cal.rule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sai.cal.model.Expression;

public class RuleValidatorTest {

	private static RuleValidator ruleEvaluator;

	@BeforeClass
	public static void init() {
		ruleEvaluator = RuleValidator.getInstance();
	}

	@Test
	public void isValid_oneRuleFailing() {
		assertFalse(ruleEvaluator.evaluate(new Expression("(2+(7+4)")));
	}

	@Test
	public void isValid_moreThanOneRuleFailing() {
		assertFalse(ruleEvaluator.evaluate(new Expression("(2+(((7+++4)")));
	}

	@Test
	public void isValid_noRuleFailing() {
		assertTrue(ruleEvaluator.evaluate(new Expression("2+(6+4)")));
	}

}
