package com.sai.cal.rule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sai.cal.model.Expression;

public class ExprWithInvalidStartOperatorRuleTest {

	private static ICalculatorRule rule;

	@BeforeClass
	public static void init() {
		rule = new ExprWithInvalidStartOperatorRule();
	}

	@Test
	public void isValid_startWithClosingRoundBracket() {
		Expression expr = new Expression(")2+(7+4)");
		assertFalse(rule.isValid(expr));
	}
	
	@Test
	public void isValid_startWithOperator() {
		Expression expr = new Expression("-2+(7+4)");
		assertFalse(rule.isValid(expr));
	}

	@Test
	public void isValid_startWithDecimalPoint() {
		Expression expr = new Expression(".2+(7+4)");
		assertFalse(rule.isValid(expr));
	}

	@Test
	public void isValid_startWithOpeningRoundBracket() {
		Expression expr = new Expression("(2+7)+4");
		assertTrue(rule.isValid(expr));
	}

}
