package com.sai.cal.rule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sai.cal.model.Expression;

public class BlankExpressionRuleTest {

	private static ICalculatorRule rule;

	@BeforeClass
	public static void init() {
		rule = new BlankExpressionRule();
	}

	@Test
	public void isValid_nullContentInExpression() {
		Expression expr = new Expression(null);
		assertFalse(rule.isValid(expr));
	}

	@Test
	public void isValid_blankExpression() {
		Expression expr = new Expression("  ");
		assertFalse(rule.isValid(expr));
	}

	@Test
	public void isValid_nonBlankExpression() {
		Expression expr = new Expression("1+2");
		assertTrue(rule.isValid(expr));
	}

}
