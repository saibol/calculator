package com.sai.cal.factory;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.sai.cal.constant.Type;
import com.sai.cal.model.Component;

public class ComponentFactoryTest {

	@Test
	public void createComponents_checkNumberOfCompCreated() {
		assertEquals(3, ComponentFactory.createComponents("7+2").size());
	}
	
	@Test
	public void createComponents_checkCompCreatedTypes() {
		List<Component> components = ComponentFactory.createComponents("7+2");
		assertEquals(Type.OPERAND, components.get(0).getType());
		assertEquals(Type.OPERATOR, components.get(1).getType());
		assertEquals(Type.OPERAND, components.get(2).getType());
	}

	@Test
	public void createComponents_singalDigit() {
		assertEquals(7, ComponentFactory.createComponents("7+(6*5)").size());
		assertEquals(5, ComponentFactory.createComponents("(6*5)").size());
	}

	@Test
	public void createComponents_sizeOfDigitMore() {
		assertEquals(9, ComponentFactory.createComponents("2+70+(6000*5)").size());
	}

}
