package com.sai.cal.rule;

import com.sai.cal.constant.CalulatorConstant;
import com.sai.cal.model.Expression;
import com.sai.cal.util.CommonUtil;

public class AdjacentOfOperandRule implements ICalculatorRule {

	@Override
	public boolean isValid(Expression expression) {

		char[] _expr = expression.getContent().toCharArray();
		for (int i = 0; i + 1 < _expr.length; i++) {
			if (CommonUtil.contains(CalulatorConstant.INVALID_EXPR_END_CHARS, _expr[i])
					&& CommonUtil.contains(CalulatorConstant.INVALID_EXPR_START_CHARS, _expr[i + 1]))
				return false;
		}
		return true;
	}
}
