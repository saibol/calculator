package com.sai.cal.rule;

import com.sai.cal.constant.CalulatorConstant;
import com.sai.cal.model.Expression;
import com.sai.cal.util.CommonUtil;

public class NoOperandRule implements ICalculatorRule {

	@Override
	public boolean isValid(Expression expression) {
		boolean isOperandPresent = false;

		for (char c : expression.getContent().toCharArray()) {
			if (CommonUtil.contains(CalulatorConstant.OPERANDS, c)) {
				isOperandPresent = true;
				break;
			}
		}
		return isOperandPresent;
	}

}
