package com.sai.cal.rule;

import com.sai.cal.constant.CalulatorConstant;
import com.sai.cal.model.Expression;
import com.sai.cal.util.CommonUtil;

public class ExprWithInvalidStartOperatorRule implements ICalculatorRule {

	@Override
	public boolean isValid(Expression expression) {
		char[] chars = expression.getContent().toCharArray();
		return !(chars.length > 0 && CommonUtil.contains(CalulatorConstant.INVALID_EXPR_START_CHARS, chars[0]));
	}
}
