package com.sai.cal.rule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.sai.cal.model.Expression;

public class RuleValidator {

	private static RuleValidator ruleExpressionEvaluator;
	private static List<ICalculatorRule> _rules = new ArrayList<>();

	static {
		ruleExpressionEvaluator = new RuleValidator();
		_rules.addAll(Arrays.asList(
				new AdjacentOfOperandRule(),
				new AdjacentToClosingBraceRule(),
				new BlankExpressionRule(),
				new ExprWithInvalidEndOperatorRule(),
				new ExprWithInvalidStartOperatorRule(),
				new NoOperandRule(),
				new OnlyOperatorAndOperandRule(),
				new OpeningBraceMatchClosingBraceRule()));
	}
	
	private RuleValidator() {}
	
	public static RuleValidator getInstance() {
		return ruleExpressionEvaluator;
	}
	
	public boolean evaluate(Expression expr) {
		return !_rules.stream()
				.filter(action -> !action.isValid(expr))
				.findAny()
				.isPresent();
	}
}
