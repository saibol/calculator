package com.sai.cal.rule;

import com.sai.cal.model.Expression;

public interface ICalculatorRule {
	boolean isValid(Expression expression);
}
