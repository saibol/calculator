package com.sai.cal.rule;

import org.apache.commons.lang3.StringUtils;

import com.sai.cal.model.Expression;

public class BlankExpressionRule implements ICalculatorRule {

	@Override
	public boolean isValid(Expression expression) {

		return !StringUtils.isBlank(expression.getContent());
	}
}
