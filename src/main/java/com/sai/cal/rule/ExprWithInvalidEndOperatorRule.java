package com.sai.cal.rule;

import com.sai.cal.constant.CalulatorConstant;
import com.sai.cal.model.Expression;
import com.sai.cal.util.CommonUtil;

public class ExprWithInvalidEndOperatorRule implements ICalculatorRule {

	@Override
	public boolean isValid(Expression expression) {
		char[] chars = expression.getContent().toCharArray();
		return !(chars.length > 0
				&& CommonUtil.contains(CalulatorConstant.INVALID_EXPR_END_CHARS, chars[chars.length - 1]));
	}
}
