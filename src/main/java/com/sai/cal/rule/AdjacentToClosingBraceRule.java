package com.sai.cal.rule;

import com.sai.cal.constant.CalulatorConstant;
import com.sai.cal.model.Expression;
import com.sai.cal.util.CommonUtil;

public class AdjacentToClosingBraceRule implements ICalculatorRule {

	@Override
	public boolean isValid(Expression expression) {

		char[] _expr = expression.getContent().toCharArray();
		for (int i = 0; i + 1 < _expr.length; i++) {
			if (CommonUtil.contains(CalulatorConstant.CLOSING_BRACE.toCharArray(), _expr[i]) && CommonUtil.contains(CalulatorConstant.INVALID_ADJC_FOR_CLOSING_BRACE, _expr[i + 1]))
				return false;
		}
		return true;
	}

}
