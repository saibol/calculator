package com.sai.cal.rule;

import com.sai.cal.constant.CalulatorConstant;
import com.sai.cal.model.Expression;
import com.sai.cal.util.CommonUtil;

public class OnlyOperatorAndOperandRule implements ICalculatorRule {

	@Override
	public boolean isValid(Expression expression) {
		for (char c : expression.getContent().toCharArray()) {
			if (!CommonUtil.contains(CalulatorConstant.VALID_EXPR_CHARS, c))
				return false;
		}
		return true;
	}

}
