package com.sai.cal.rule;

import org.apache.commons.lang3.StringUtils;

import com.sai.cal.constant.CalulatorConstant;
import com.sai.cal.model.Expression;

public class OpeningBraceMatchClosingBraceRule implements ICalculatorRule {

	@Override
	public boolean isValid(Expression expression) {
		return StringUtils.countMatches(expression.getContent(), CalulatorConstant.OPENING_BRACE) == StringUtils
				.countMatches(expression.getContent(), CalulatorConstant.CLOSING_BRACE);
	}

}
