package com.sai.cal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.apache.log4j.Logger;

import com.sai.cal.bm.RuleBM;
import com.sai.cal.constant.CalulatorConstant;
import com.sai.cal.exception.InvalidLimitException;
import com.sai.cal.model.Expression;
import com.sai.cal.model.RuleResult;

public class StringCalculatorApp {

	static Logger logger = Logger.getLogger(StringCalculatorApp.class);
	
	/**
	 * entry point of app - this will display console and get expression from user that need to evaluate
	 * @param args
	 */
	public static void main(String[] args) {
		List<Expression> exprs = new ArrayList<>();
		RuleBM ruleBM = RuleBM.getInstance();
		
		try {
			logger.info("Please Enter number of test cases to execute : ");
			int limitVal = Integer.parseInt(readNextData());

			if (!isValidLimit(limitVal))
				throw new InvalidLimitException("Invalid limit passed. Provide value only upto 2 decimals");

			logger.info(String.format("Enter %d Expressions : ", limitVal));
			IntStream.range(0, limitVal).forEach(x -> {
				exprs.add(new Expression(readNextData()));
			});

		} catch (InvalidLimitException ile) {
			logger.error(ile.getMessage());
		} catch (Exception e) {
			logger.error("Invalid argument passed");
		}
		ruleBM.display(ruleBM.process(exprs));
	}

	/**
	 * check how many expression user can provide at a given time
	 * @param val
	 * @return
	 */
	private static boolean isValidLimit(int val) {
		return val >= CalulatorConstant.MIN_LIMIT && val <= CalulatorConstant.MAX_LIMIT;
	}

	/**
	 * read next expression
	 * @return
	 */
	private static String readNextData() {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try {
			return reader.readLine();
		} catch (IOException e) {
			logger.error("Exception occurred while reading argument passed");
			e.printStackTrace();
		}
		return null;
	}
}
