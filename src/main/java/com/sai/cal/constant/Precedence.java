package com.sai.cal.constant;

public enum Precedence {
	OP_BRACE("(", 1), 
	CL_BRACE(")", 1),
	POW("^", 2), 
    MUL("*", 3), 
    DIV("/", 3),
    ADD("+", 4),
    SUB("-", 4);
    
    private final String sign;
    private final Integer priority;

    public String getSign() {
		return sign;
	}

	public Integer getPriority() {
		return priority;
	}

	Precedence(String sign, Integer priority) {
        this.sign = sign;
        this.priority = priority;
    }
	
	public static Precedence getPrecedence(String sign) {
		for (Precedence p : values()) { 
	        if(p.getSign().equals(sign))
	        	return p;
	    } 
		return null;
	}
}