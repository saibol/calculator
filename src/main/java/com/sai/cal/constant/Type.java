package com.sai.cal.constant;

public enum Type {
	OPERAND, OPERATOR, OPENING_BRACE, CLOSING_BRACE
}