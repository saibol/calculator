package com.sai.cal.constant;

public class CalulatorConstant {
	public static final String LOG_FILE = "log4j.xml";
	public static final char[] VALID_EXPR_CHARS = new char[]{'+','-','*','/','^','(',')','1','2','3','4','5','6','7','8','9','0'};
	public static final char[] OPERANDS = new char[]{'1','2','3','4','5','6','7','8','9','0'};
	public static final char[] INVALID_EXPR_START_CHARS = new char[]{'+','-','*','/','^',')','.'};
	public static final char[] INVALID_EXPR_END_CHARS = new char[]{'+','-','*','/','^','(','.'};
	public static final char[] INVALID_ADJC_FOR_CLOSING_BRACE = new char[]{'(','.'};
	public static final String OPENING_BRACE = "(";
	public static final String CLOSING_BRACE = ")";
	public static final int MIN_LIMIT = 1;
	public static final int MAX_LIMIT = 99;
	public static final String MSG_EXPR_INVALID = "INVALID EXPRESSION";
}
