package com.sai.cal.bm;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.sai.cal.constant.CalulatorConstant;
import com.sai.cal.factory.ComponentFactory;
import com.sai.cal.model.Component;
import com.sai.cal.model.Expression;
import com.sai.cal.model.Number;
import com.sai.cal.model.RuleResult;
import com.sai.cal.rule.RuleValidator;

public class RuleBM {

	public final static Logger LOGGER = Logger.getLogger(RuleBM.class);
	private RuleValidator ruleEvaluator = RuleValidator.getInstance();
	private EvaluatorBM evaluatorBM = EvaluatorBM.getInstance();
	private static final RuleBM ruleBM = new RuleBM();

	private RuleBM() {}
	
	public List<RuleResult> process(List<Expression> expressions) {

		List<RuleResult> results = new ArrayList<>();
		expressions.stream().forEach(_expr -> {
			results.add(
					new RuleResult(_expr, ruleEvaluator.evaluate(_expr) 
							? Integer.toString(execute(_expr.getContent()))
							: CalulatorConstant.MSG_EXPR_INVALID));
			});
		return results;
	}
	
	public void display(List<RuleResult> _results) {
		for (int i = 0; i < _results.size(); i++) {
			LOGGER.info(String.format("Case #%d: %s", i + 1, _results.get(i).getResult()));
		}
	}

	public Integer execute(String exprStr) {
		List<Component> infixComps = ComponentFactory.createComponents(exprStr);
//		LOGGER.info(evaluatorBM.getExpr(infixComps));
		List<Component> postfixComps = evaluatorBM.infxToPostfx(infixComps);
//		LOGGER.info(evaluatorBM.getExpr(postfixComps));
		Integer result = ((Number) evaluatorBM.evaluate(postfixComps)).getNumber();
//		LOGGER.info("Result of Expression : "+result);
		return result; 
	}
	
	/**
	 * singleton with egar initialization
	 * @return
	 */
	public static RuleBM getInstance() {
		return ruleBM;
	}
}
