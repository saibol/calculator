package com.sai.cal.bm;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.apache.log4j.Logger;

import com.sai.cal.constant.Type;
import com.sai.cal.exception.InvalidOperatorException;
import com.sai.cal.model.Component;
import com.sai.cal.model.Number;
import com.sai.cal.model.Operator;

public class EvaluatorBM {
	public final static Logger LOGGER = Logger.getLogger(EvaluatorBM.class.getName());
	private final static EvaluatorBM evaluatorBM = new EvaluatorBM(); 
	
	private EvaluatorBM() {}
	
	/**
	 * get components expression in format
	 * @param comps
	 */
	public String getExpr(List<Component> comps) {
		StringBuilder _msg = new StringBuilder();
		comps.stream().forEach(x -> {
			_msg.append((Type.OPERAND.equals(x.getType())) 
					? ((Number) x).getNumber() 
					: ((Operator) x).getSign());
		});
		return _msg.toString();
	}
	
	/**
	 * converts infix expression to postfix
	 * @param comps - flow in infix form
	 * @return
	 */
	public List<Component> infxToPostfx(List<Component> comps) {
		Stack<Component> stk = new Stack<>();
		List<Component> postfixComps = new ArrayList<>();
		comps.forEach(cmp -> {
			if(Type.OPERAND.equals(cmp.getType())) {
				postfixComps.add(cmp);
			}else {
				if(Type.CLOSING_BRACE.equals(cmp.getType())) {
					while(!stk.isEmpty() && !Type.OPENING_BRACE.equals(stk.peek().getType())) {
						postfixComps.add(stk.pop());
					}
					stk.pop();	// for removing '(' from stack
				}else {
					while(!stk.isEmpty() && !Type.OPENING_BRACE.equals(stk.peek().getType()) 
							&& hasStkHigherPriorityEle(cmp,stk.peek())){
						postfixComps.add(stk.pop());
					}
					stk.push(cmp);
				}
			}
		});
		while(!stk.isEmpty())
			postfixComps.add(stk.pop());
		return postfixComps;
	}
	
	/**
	 * Evaluates postfix expression provided
	 * @param comps - expression details in postfix form
	 * @return
	 */
	public Component evaluate(List<Component> comps){
		Stack<Component> stk = new Stack<>();
		comps.forEach(cmp -> {
			if(Type.OPERAND.equals(cmp.getType())) {
				stk.push(cmp);
			}else {
				Operator op = (Operator) cmp;
				Number num1 = (Number)stk.pop();
				Number num2 = (Number)stk.pop();
				stk.push(new Number(calculate(op,num1,num2)));
			}
		});
		return stk.pop();
	}
	
	/**
	 * perform basic math calculation for given two number
	 * @param op perform what calculation you want to perfome
	 * @param num1 - first number
	 * @param num2 - second number
	 * @return - result of calculation
	 */
	public Integer calculate(Component op, Component _num1, Component _num2) {
		
		String sign = ((Operator)op).getSign();
		Integer num1 = ((Number)_num1).getNumber();
		Integer num2 = ((Number)_num2).getNumber();
		
		try {
			if("^".equals(sign))
				return (int)Math.pow(num2,num1);
			else if("*".equals(sign))
				return num2 * num1;
			else if("/".equals(sign))
				return num2 / num1;
			else if("+".equals(sign))
				return num2 + num1;
			else if("-".equals(sign))
				return num2 - num1;
			else 
				throw new InvalidOperatorException("Invalid Operator");
		}catch (ArithmeticException | InvalidOperatorException ex) {
			LOGGER.error(ex.getMessage());
		}
		return null;
	}
	
	/**
	 * check if which operator has higher priority and need to execute first
	 * @param _cur - current component
	 * @param _lastInStk - component from stack
	 * @return - return true/false
	 */
	public boolean hasStkHigherPriorityEle(Component _cur, Component _lastInStk) {
		if(hasSameType(_cur,_lastInStk) && hasSameSign(_cur,_lastInStk) && hasSign(_cur, "^"))
			return false;
		return ((Operator)_lastInStk).getPriority() <= ((Operator)_cur).getPriority();
	}
	
	private static boolean hasSameType(Component _cmp1, Component _cmp2) {
		return _cmp1.getType().equals(_cmp2.getType());
	}
	
	private static boolean hasSameSign(Component _cmp1, Component _cmp2) {
		return ((Operator)_cmp1).getSign().equals(((Operator)_cmp2).getSign());
	}
	
	private static boolean hasSign(Component _cmp1, String sign) {
		return ("^".equals(((Operator)_cmp1).getSign()));
	}
	
	/**
	 * singleton with eager initialization
	 * @return
	 */
	public static EvaluatorBM getInstance() {
		return evaluatorBM;
	}
}
