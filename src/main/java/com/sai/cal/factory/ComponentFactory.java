package com.sai.cal.factory;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.sai.cal.model.ClosingBrace;
import com.sai.cal.model.Component;
import com.sai.cal.model.Number;
import com.sai.cal.model.OpeningBrace;
import com.sai.cal.model.Operator;

public class ComponentFactory {

	public static Component getComponent(String code) {
		switch (code) {
		case ")":
			return new ClosingBrace(code);
		case "(":
			return new OpeningBrace(code);
		case "+":
		case "-":
		case "*":
		case "/":
		case "^":
			return new Operator(code);
		default:
			return new Number(code);
		}
	}
	
	public static List<Component> createComponents(String exprStr) {
		for (String s : Arrays.asList("\\(","\\)", "\\^", "\\*", "\\/", "\\+", "\\-"))
			exprStr = exprStr.replaceAll(s, "," + s + ",");

		return Arrays.asList(exprStr.split(",")).stream().filter(StringUtils::isNoneBlank)
				.map(ComponentFactory::getComponent).collect(Collectors.toList());
	}
}