package com.sai.cal.model;

import com.sai.cal.constant.Type;

public class OpeningBrace extends Operator {
	public OpeningBrace(String sign) {
		super(sign);
		setType(Type.OPENING_BRACE);
	}

	@Override
	public String toString() {
		return "OpeningBrace [sign=" + getSign() + "]";
	}
}