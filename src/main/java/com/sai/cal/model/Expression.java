package com.sai.cal.model;

public class Expression {
	private String content;

	public Expression(String content) {
		setContent(content);
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = (content != null) ? content.replaceAll("\\s", "") : "";
	}
}
