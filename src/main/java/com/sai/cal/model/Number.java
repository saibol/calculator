package com.sai.cal.model;

import com.sai.cal.constant.Type;

public class Number extends Component {
	private Integer number;

	public Number(String number) {
		this.number = Integer.valueOf(number);
		setType(Type.OPERAND);
	}

	public Number(Integer number) {
		this.number = number;
		setType(Type.OPERAND);
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return "Number [number=" + number + "]";
	}
}