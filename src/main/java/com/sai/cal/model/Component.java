package com.sai.cal.model;

import com.sai.cal.constant.Type;

public abstract class Component {
	private Type type;

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
}