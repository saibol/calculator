package com.sai.cal.model;

import com.sai.cal.constant.Type;

public class ClosingBrace extends Operator {
	public ClosingBrace(String sign) {
		super(sign);
		setType(Type.CLOSING_BRACE);
	}

	@Override
	public String toString() {
		return "ClosingBrace [sign=" + getSign() + "]";
	}
}