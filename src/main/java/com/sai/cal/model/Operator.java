package com.sai.cal.model;

import com.sai.cal.constant.Precedence;
import com.sai.cal.constant.Type;

public class Operator extends Component {
	protected String sign;
	protected Integer priority;

	public Operator(String sign) {
		this.sign = sign;
		setType(Type.OPERATOR);
		assignPriority();
	}

	private void assignPriority() {
		this.priority = Precedence.getPrecedence(getSign()).getPriority();
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
	
	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {
		return "Operator [sign=" + sign + "]";
	}
}