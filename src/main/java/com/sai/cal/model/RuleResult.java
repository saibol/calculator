package com.sai.cal.model;

public class RuleResult {
	private Expression expression;
	private String result;

	public RuleResult(Expression expression, String result) {
		super();
		this.expression = expression;
		this.result = result;
	}

	public Expression getExpression() {
		return expression;
	}

	public void setExpression(Expression expression) {
		this.expression = expression;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
