package com.sai.cal.util;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.sai.cal.constant.CalulatorConstant;

public class Log4jInit {
	private final static Logger LOGGER = Logger.getLogger(Log4jInit.class.getName());
	private CommonUtil commonUtil = CommonUtil.getInstance();
	
	public void init() {
		DOMConfigurator.configure(commonUtil.getFile(CalulatorConstant.LOG_FILE)
				.getAbsolutePath());
		LOGGER.info("Logger Initialized");
	}
}
