package com.sai.cal.util;

import java.io.File;

public class CommonUtil {
	private static final CommonUtil commonUtil = new CommonUtil();
	
	private CommonUtil() {}
	
	public File getFile(String fileName){
		ClassLoader classLoader = getClass().getClassLoader();
		return new File(classLoader.getResource(fileName).getFile());
	}
	
	public static CommonUtil getInstance() {
		return commonUtil;
	}
	
	public static boolean contains(char[] charArray, char _s) {
		boolean contains = false;
		for (char c : charArray) {
			if (c == _s) {
				contains = true;
				break;
			}
		}
		return contains;
	}
}
