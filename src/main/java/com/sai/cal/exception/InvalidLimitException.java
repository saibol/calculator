package com.sai.cal.exception;

public class InvalidLimitException extends Exception{

	private static final long serialVersionUID = 1L;

	public InvalidLimitException(String message){
		super(message);
	}

}
